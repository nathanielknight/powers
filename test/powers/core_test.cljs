(ns powers.core-test
  (:require [powers.core :as pc]
            [powers.game :as pg]
            [om.core :as om :include-macros true]
            [powers.repl]
            [cljs.core]))

(enable-console-print!)


(def app-state pc/app-state)

;; Sample Boards and Helpers ============
(def nil-board (zipmap pg/blank-board-keys (repeat nil)))
(def two-board (zipmap pg/blank-board-keys (repeat 2)))
(def count-board (zipmap pg/blank-board-keys (range 16)))

(defn pretty-board [b]
  (for [y [0 1 2 3]]
    (str (for [x [0 1 2 3]] (b [x,y])) "\n")))


;; Test Functions ================================
(defn test-pair [g f x]
  (println "testing inverses =================")
  (println (pretty-board x))
  (println (g x))
  (println (pretty-board  (f (g x))))
  (= x (-> x g f)))

;; Execute! ===========================
(defn run-tests []
  (assert (test-pair pg/>left-rows pg/<left-rows count-board)
          "<>left are inverses")
  (assert (test-pair pg/>right-rows pg/<right-rows count-board )
          "<>right are inverses")
  (assert (test-pair pg/>up-rows pg/<up-rows count-board)
          "<>up are inverses")
  (assert (test-pair pg/>down-rows pg/<down-rows count-board)
          "<>down are inverses")
)

(println "Running tests")
(run-tests)
(println "Tests complete!")

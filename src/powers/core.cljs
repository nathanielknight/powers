
(ns powers.core
  (:require-macros [cljs.core.async.macros :refer (go)])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan <!]]
            [powers.game :as g]
            [powers.repl]))

(enable-console-print!)



;; Global State and Constants ===================================

(def app-state (atom {:board (zipmap g/blank-board-keys (repeat nil))
                      :score 0}))


(defn pretty-board [b]
  (for [y [0 1 2 3]]
    (str (for [x [0 1 2 3]] (b [x,y])) "\n")))

;; Display ======================================================
(defn score-view [app owner]
  (reify om/IRenderState
    (render-state [_ state]
      (dom/div #js {:id "score"} (:score app)))))


(defn cell-view [cell owner]
  (reify
    om/IRender
    (render [_]
      (if (nil? cell)
        (dom/div #js {:className "cell"} "")
        (dom/div #js {:className (str "cell cell-" (int cell))} (str cell))))))

(defn row-view [row owner]
  (reify
    om/IRender
    (render [_]
      (apply dom/div #js {:className "row"} (om/build-all cell-view row)))))

(defn board-view [app owner]
  (reify
    om/IRender
    (render [_]
      (apply dom/div nil
             (om/build-all row-view
                           (for [y (range 4)]
                             (vec (for [x (range 4)]
                                    ((app :board) [x,y])))))))))

    
(defn render-game [app-state owner]
  (reify
    om/IInitState
    (init-state [_]
      {:input (chan)})
    om/IWillMount
    (will-mount [_]
      (om/update! app-state :board (g/new-board))
      (let [input (om/get-state owner :input)]
        (go (loop []
              (let [key (<! input)]
                (let [f (g/action-for key)]
                  (om/transact! app-state :board f))
              (recur))))))
    om/IRenderState
    (render-state [_ {:keys [input]}]
      (dom/div #js {:id "game"
                    :tabIndex 0
                    :onKeyDown (fn [e]
                                 (put! input (.-key e)))}
               (om/build score-view app-state)
               (om/build board-view app-state)))))


;; Om Root=======================================================
(om/root
 render-game
 app-state
 {:target (. js/document (getElementById "app"))})
(set! (.-onload js/window)
      (fn []
        (.  (. js/document (getElementById "game")) (focus))
))

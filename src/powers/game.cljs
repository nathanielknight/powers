(ns powers.game)

(enable-console-print!)


(def blank-board-keys
  (vec (for [y [0 1 2 3]
        x [0 1 2 3]]
         [x y])))


(defn blank-board []
  (apply assoc {} (interleave blank-board-keys (repeat nil))))


(defn spawn-tile []
  (if (> (rand) 0.8) 4 2))

(defn random-cell
  ([] rand-nth blank-board-keys)
  ([board] (loop [test [(rand-int 4) (rand-int 4)]]
           (if (nil? (get board test))
             test
             (recur [(rand-int 4) (rand-int 4)])))))

(defn add-random [board]
  (assoc board (random-cell board) (spawn-tile)))

(defn new-board []
  (-> (blank-board)
      (add-random)
      (add-random)))

;; (defn new-board []
;;   (zipmap blank-board-keys (range 16)))

(defn pack-row [row]
  (->> row
       (filter (fn [x] (not (nil? x))))
       ((fn [nums]
         (loop [[a b & rest :as nums] nums
                result []]
           (cond
            (nil? a) result
            (nil? b) (concat result [a])
            (= a b) (recur (nnext nums) (concat result [(+ a b)]))
            :else (recur (next nums) (concat result [a])))))) 
       (flatten)
       ((fn [x] (concat x [nil nil nil nil])))
       (take 4)))


(defn >left-rows [board]
  (for [y [0 1 2 3]]
    (vec (for [x [0 1 2 3 ]] (get board [x,y])))))
(defn >right-rows [board]
  (for [y [0 1 2 3]]
    (vec (for [x [3 2 1 0]] (board [x,y])))))
(defn >up-rows [board]
  (for [x [0 1 2 3]]
    (vec (for [y [0 1 2 3]] (board [x,y])))))
(defn >down-rows [board]
  (for [x [0 1 2 3]]
    (vec (for [y [3 2 1 0]] (board [x,y])))))

(defn <left-rows [rows]
  (zipmap (for [y [0 1 2 3], x [0 1 2 3]] [x,y]) (flatten rows)))
(defn <right-rows [rows]
  (zipmap (for [y [0 1 2 3], x [3 2 1 0]] [x,y]) (flatten rows)))
(defn <up-rows [rows]
  (zipmap (for [x [0 1 2 3], y [0 1 2 3]] [x,y]) (flatten rows)))
(defn <down-rows [rows]
  (zipmap (for [x [0 1 2 3], y [3 2 1 0]] [x,y]) (flatten rows)))


(defn make-mover [in out]
  (fn [board]
    (->> board
         (in )
         (map pack-row)
         (outE))))

(defn make-move [in out]
  (fn [board]
    (let [orig board]
      (->> board
           (in)
           (map (fn [x] (let [g (pack-row x)] g)))
           (out)
           ((fn [b]
             (if (= b orig) b (add-random b))))))))

;; (defn game-move [board mover]
;;   (let [orig board]
;;     ))

(def left (make-move >left-rows <left-rows))
(def right (make-move >right-rows <right-rows))
(def up (make-move >up-rows <up-rows))
(def down (make-move >down-rows <down-rows))

(defn action-for [k]
  (cond
   (= k "ArrowLeft") left
   (= k "ArrowRight") right
   (= k "ArrowUp") up
   (= k "ArrowDown") down
   :else identity))



